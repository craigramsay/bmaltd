<?php
/**
 * The template part for displaying ACF flexible content.
 */

if ( have_rows( 'blocks' ) ):

    while ( have_rows( 'blocks' ) ) : the_row();

        switch ( get_row_layout() ) {

			case 'text_block': ?>

				<div class="col">
					<div class="col-item col-item-full col-item--margin-bottom-20">
						<?php if ( get_sub_field( 'header' ) ) { ?>
							<h3 class="uppercase" itemprop="name"><?php echo esc_attr( get_sub_field( 'header' ) ); ?></h3>
						<?php } ?>					
						<span itemprop="articleSection">
							<?php the_sub_field( 'content' ); ?>
						</span>
					</div>
				</div>

				<?php break;			

			case 'gallery_block':

				?>
				<!-- Photos On Page -->
				<div class="col col--margin-bottom-20">
					<?php do_shortcode( '[malinky-gallery malinky_col_item="col-item col-item-quarter col-item-half--small"]' ); ?>
				</div>
				<?php

				break;				

        }

    endwhile;

else :

endif;

?>