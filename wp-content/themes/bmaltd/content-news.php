<?php
/**
 * The template part for displaying lists of news items.
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'archive-content col-item col-item-full col-item--margin-bottom-20 malinky-fade-in-long-delay' ); ?> itemscope itemtype="http://schema.org/BlogPosting">
	<meta itemprop="url" content="<?php echo esc_url( get_permalink() ); ?>" />
	<meta itemprop="name" content="<?php echo esc_attr( get_the_title() ) ; ?>" />
	<div class="archive-content-title">
		<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><h4 class="archive-content-title__title no-margin entry-title"><?php the_title(); ?></h4></a>
		<div class="content-header__meta">
			<?php echo malinky_content_meta( false, false ); ?>
		</div><!-- .content-header__meta .col -->
	</div>
	<?php the_excerpt(); ?>
	<?php echo malinky_content_hatom_footer(); ?>
</article>