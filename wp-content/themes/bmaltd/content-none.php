<?php
/**
 * The template part for no posts to display in archives.php, index.php and search.php
 */
?>
<article>
	<header class="content-header">
		<h1 class="content-header__title uppercase">Nothing Found</h1>
	</header><!-- .content-header -->

	<div class="content-main">
		<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', 'malinky' ); ?></p>
	</div><!-- .content-main -->
</article>