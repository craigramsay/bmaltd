<?php
/**
 * The sidebar.
 */

if ( is_active_sidebar( 'sidebar' ) ) { ?>
	<div class="sidebar">
		<?php dynamic_sidebar( 'sidebar' ); ?>
	</div>
<?php }

if ( is_active_sidebar( 'sidebar-mobile' ) ) { ?>
	<div class="sidebar-mobile">
		<a class="widget-open heading-3">More</a>
		<?php dynamic_sidebar( 'sidebar-mobile' ); ?>
	</div>
<?php } ?>