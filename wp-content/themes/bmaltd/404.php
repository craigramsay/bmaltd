<?php
/**
 * The template for displaying 404 pages.
 */

get_header(); ?>

<main role="main" class="wrap wrap-mobile">
	<div class="col">
		<div class="col-item col-item-full">
			<article>
				<header class="content-header">
					<h1 class="content-header__title uppercase">Nothing Found</h1>
				</header><!-- .content-header -->
				<div class="content-main">
					<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', 'malinky' ); ?></p>
				</div><!-- .content-main -->
			</article>
		</div>
	</div><!-- .col -->
</main><!-- .main -->

<?php get_footer(); ?>