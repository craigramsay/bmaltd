<?php
/**
 * The template part for displaying lists of posts when no post_format() is set.
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'archive-content col-item col-item-half col-item-full--small col-item--margin-bottom-20 malinky-fade-in-long-delay' ); ?> itemscope itemtype="http://schema.org/BlogPosting">
	<meta itemprop="url" content="<?php echo esc_url( get_permalink() ); ?>" />
	<meta itemprop="name" content="<?php echo esc_attr( get_the_title() ) ; ?>" />
	<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark">
		<img src="<?php echo esc_url( malinky_acf_image_array( 'project_hero_shot', 'malinky_thumbnail' ) ); ?>" alt="<?php echo esc_attr( get_the_title() ); ?>" class="archive-content__image" itemprop="image" />
	</a>
	<div class="archive-content-title">
		<?php if ( get_field ( 'project_title' ) ) { ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><h4 class="archive-content-title__title no-margin entry-title"><?php echo esc_html( get_field( 'project_title' ) ); ?></h4></a>
			<?php if ( get_field ( 'project_sub_title' ) ) { ?>
				<h5 class="archive-content-title__sub no-margin"><?php echo esc_html( get_field( 'project_sub_title' ) ); ?></h5>
			<?php }
		} else { ?>
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><h3 class="no-margin entry-title"><?php echo esc_html( get_the_title() ); ?></h3></a>
		<?php } ?>
	</div>
	<?php echo malinky_content_hatom_footer(); ?>
</article>