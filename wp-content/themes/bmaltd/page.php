<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

<main role="main" class="wrap wrap-mobile">

	<?php while ( have_posts() ) : the_post(); ?>

		<div class="col">
			<div class="col-item col-item-full">
				<?php get_template_part( 'content', 'page' ); ?>
			</div>
		</div>

	<?php endwhile; //end loop. ?>

</main><!-- #main -->
	
<?php get_footer(); ?>