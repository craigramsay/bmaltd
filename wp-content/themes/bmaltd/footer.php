<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Malinky Media
 */
?>

<footer class="main-footer wrap-1190" role="contentinfo">
	<div class="wrap">
		<div class="col">
			<div class="col-item col-item-half">
				<?php echo Malinky_Settings_Plugin::malinky_settings_get_option('_000001_contact_information-address'); ?>	
			</div><!--
			--><div class="col-item col-item-half col--align-right col-item--bottom">
				<p class="main-footer__phone no-margin"><?php echo esc_html( Malinky_Settings_Plugin::malinky_settings_get_option('_000001_contact_information-phone_number') ); ?></p>
				<p class="main-footer__phone no-margin"><?php echo Malinky_Settings_Plugin::malinky_settings_get_option('_000001_contact_information-mobile_number'); ?></p>
				<p class="main-footer__email no-margin"><?php echo esc_html( Malinky_Settings_Plugin::malinky_settings_get_option('_000001_contact_information-email_address') ); ?></p>
				<p class="main-footer__social no-margin"><a href="<?php echo esc_attr( Malinky_Settings_Plugin::malinky_settings_get_option('_000001_contact_information-facebook_account') ); ?>" target="_blank">Facebook</a></p>
				<p class="main-footer__social no-margin"><a href="<?php echo esc_attr( Malinky_Settings_Plugin::malinky_settings_get_option('_000001_contact_information-twitter_account') ); ?>" target="_blank">Twitter</a></p>
			</div>
		</div>
	</div>
</footer><!-- .main-footer -->

<footer class="sub-footer wrap" role="contentinfo">

	<div class="col">
		<div class="col-item col-item-full sub-footer__copyright">
			<?php echo bloginfo( 'name' ); ?> &copy; <?php echo date('Y'); ?>
		</div>
	</div>

</footer><!-- .main-footer -->

<a class="back-top image-font__fontawesome fa-chevron-up"></a>

<?php if ( WP_ENV == 'prod' ) { ?>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create',"<?php echo get_option( '_000003_google_analytics' ); ?>",'auto');ga('send','pageview');
</script>

<?php } ?>
		
<?php wp_footer(); ?>

<?php if ( WP_ENV == 'local' ) { ?>

<!-- Live Reload -->
<script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1"></' + 'script>')</script>

<?php } ?>

</body>
</html>