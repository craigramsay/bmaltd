<?php
/**
 * The template for displaying all archive pages.
 *
 * This can be overwritten with author.php, category.php, taxonomy.php, date.php, tag.php.
 */

get_header(); ?>

<main role="main" class="wrap wrap-mobile">

	<div class="col">

		<div class="col-item col-item-3-10 col-item-full--medium col-item-full--small">
		
			<?php get_sidebar(); ?>

		</div><!--

		--><div class="col-item col-item-7-10 col-item-full--medium col-item-full--small" itemscope itemtype="http://schema.org/Blog">
			
			<?php if ( have_posts() ) { ?>

				<div class="col">
					<div class="col-item col-item-full">
						<header class="archive-introduction">
							<h1 class="archive-introduction__title entry-title uppercase" itemprop="name"><?php malinky_archive_title(); ?></h1>
							<p class="archive-introduction__description" itemprop="about"><?php malinky_archive_description(); ?></p>
						</header><!-- .content-header -->
					</div>
				</div>

				<div class="col">
				
					<?php while ( have_posts() ) : the_post(); 

						get_template_part( 'content', 'news' );

					endwhile; //end loop. ?>

				</div><!-- end .col nested -->

				<?php malinky_posts_pagination();

			} else { ?>

				<div class="col">
					<div class="col-item col-item-full">
						<?php get_template_part( 'content', 'none' ); ?>
					</div>
				</div>				

			<?php } // have_posts() ?>
			
		</div>

	</div><!-- .col -->

</main><!-- .main -->

<?php get_footer(); ?>