
<?php
/**
 * The template part for displaying a single post in single.php.
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/BlogPosting">

	<header class="content-header col--margin-bottom-20">
		<h1 class="content-header__title entry-title uppercase" itemprop="name"><?php the_title(); ?></h1>
		<meta itemprop="url" content="<?php echo esc_url( get_permalink() ); ?>" />
		<div class="content-header__meta">
			<?php echo malinky_content_meta( false, false ); ?>
		</div><!-- .content-header__meta .col -->
	</header><!-- .content-header -->

	<div class="content-main">

		<span itemprop="articleBody">
			<?php the_content(); ?>
		</span>

		<!-- Project Gallery -->
		<div class="col col--margin-bottom-20">
			<?php do_shortcode( '[malinky-post-slider images_per_slide=8]' ); ?>
		</div>

		<!-- Project table -->
		<?php if ( have_rows( 'project_table' ) ) { ?>

			<div class="col">
				<div class="col-item col-item-full">
					<h3 class="uppercase"><?php echo esc_html( get_field( 'project_table_header' ) ); ?></h3>
					<table class="malinky-table">
						<tr>
							<td>Completed</td>
							<td><?php echo malinky_content_meta( false, false, true ); ?>
							</td>
						</tr>
						<?php while ( have_rows( 'project_table' ) ) : the_row(); ?>
							<tr>
								<td><?php echo esc_html( get_sub_field( 'column_1' ) ); ?></td>
								<td><?php echo esc_html( get_sub_field( 'column_2' ) ); ?></td>
							</tr>
						<?php endwhile; ?>							
					</table>
				</div>
			</div><!-- .col -->
		<?php } ?>

	</div><!-- .content-main -->
	
	<?php echo malinky_content_hatom_footer(); ?>	
	
</article><!-- #post-## -->