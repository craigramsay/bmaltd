<?php
/**
 * The template for displaying all pages.
 */

get_header();

if ( have_rows( 'carousel_images' ) ) {

	$carousel_count = 0;

    while ( have_rows( 'carousel_images' ) ) : the_row();

        $carousel_images = get_sub_field( 'carousel_image' );
        $carousel[ $carousel_count ]['large'] 	= $carousel_images['sizes']['malinky_carousel_large'];
        $carousel[ $carousel_count ]['medium'] 	= $carousel_images['sizes']['malinky_carousel_medium'];
        $carousel[ $carousel_count ]['small'] 	= $carousel_images['sizes']['malinky_carousel_small'];
        $carousel[ $carousel_count ]['heading'] = get_sub_field( 'carousel_image_heading' );
        $carousel[ $carousel_count ]['text'] 	= get_sub_field( 'carousel_image_text' );

	$carousel_count++;

    endwhile;

    if ( malinky_is_phone() ) {
    	$carousel_size = 'small';
	} elseif ( malinky_is_tablet() ) {
		$carousel_size = 'medium';
	} else {
		$carousel_size = 'medium';
	} ?>

	<?php if ( count( get_field( 'carousel_images' ) ) > 1 ) { ?>
	<div class="wrap-1190 wrap-mobile--carousel">
		<div id="owl-carousel-id">
			<?php $carousel_count = 0;
		    while ( have_rows( 'carousel_images' ) ) : the_row(); ?>
			<div>
				<img src="<?php echo esc_url( $carousel[ $carousel_count ][ $carousel_size ] ); ?>" class="malinky-fade-in" alt="BMA Ltd Carousel" />
				<div class="owl-carousel-text malinky-fade-in-long-delay">
					<?php if ( $carousel[ $carousel_count ]['heading'] != '' ) { ?>
						<h3 class="owl-carousel-text__content uppercase"><?php echo esc_html( $carousel[ $carousel_count ]['heading'] ); ?></h3>
					<?php } ?>
					<?php if ( $carousel[ $carousel_count ]['text'] != '' ) { ?>
						<p class="owl-carousel-text__content"><?php echo esc_html( $carousel[ $carousel_count ]['text'] ); ?></p>
					<?php } ?>
				</div>
			</div>
			<?php $carousel_count++;
			endwhile; ?>
		</div>
		<div class="owl-carousel-bottom malinky-fade-in">
			<div class="wrap">
				<p class="owl-carousel-bottom__text malinky-fade-in-long-delay"><?php echo get_bloginfo( 'description' ); ?></p>
			</div>
		</div>
	</div>
	<?php } else { ?>
		<div class="wrap-1190 wrap-mobile--carousel">
			<?php $carousel_count = 0;
			while ( have_rows( 'carousel_images' ) ) : the_row(); ?>
				<div>
					<img src="<?php echo esc_url( $carousel[ $carousel_count ][ $carousel_size ] ); ?>" class="malinky-fade-in" alt="<?php echo esc_attr( bloginfo( 'name' ) ); ?> Carousel" />
					<div class="owl-carousel-text malinky-fade-in-long-delay">
						<?php if ( $carousel[ $carousel_count ]['heading'] != '' ) { ?>
							<h3 class="owl-carousel-text__content uppercase"><?php echo esc_html( $carousel[ $carousel_count ]['heading'] ); ?></h3>
						<?php } ?>
						<?php if ( $carousel[ $carousel_count ]['text'] != '' ) { ?>
							<p class="owl-carousel-text__content"><?php echo esc_html( $carousel[ $carousel_count ]['text'] ); ?></p>
						<?php } ?>
					</div>
				</div>
			<?php endwhile; ?>
			<div class="owl-carousel-bottom malinky-fade-in">
				<div class="wrap">
					<p class="owl-carousel-bottom__text malinky-fade-in-long-delay"><?php echo get_bloginfo( 'description' ); ?></p>
				</div>
			</div>
		</div>
	<?php }	

} ?>

<main role="main" class="wrap">

	<?php if ( have_rows( 'services' ) ) { ?>

	<div class="col">

	    <?php while ( have_rows( 'services' ) ) : the_row(); ?><div class="col-item col-item-third col-item-full--medium col-item-full--small">
			<div class="col service-block">
				<div class="col-item col-item-full col-item-2-5--medium col-item-full--small">
					<img src="<?php echo esc_url( malinky_image_url( get_sub_field( 'service_image' ), 'malinky_home_service' ) ); ?>" class="service-block__image service-block__image--medium" alt="<?php esc_html( get_sub_field( 'service_heading' ) ); ?>" />
				</div><!--
				--><div class="col-item col-item-full col-item-3-5--medium col-item-full--small">
					<h2 class="service-block__heading uppercase"><?php echo esc_html( get_sub_field( 'service_heading' ) ); ?></h2>
					<?php the_sub_field( 'service_content' ); ?>
					<a href="<?php echo esc_url( get_sub_field( 'service_link' ) ); ?>" class="button service-block__button">Read More</a>
				</div>
			</div><!-- .col nested -->
		</div><?php endwhile; ?>
	
	</div><!-- .col -->

	<hr>

	<?php } ?>

	<div class="col col--relative">
		<div class="col-item col-item-6-10 col-item-full--medium col-item-full--small">
		<?php if ( have_rows( 'text_blocks' ) ) { ?>
	    	<?php while ( have_rows( 'text_blocks' ) ) : the_row(); ?>
	    		<h3 class="uppercase"><?php echo esc_html( get_sub_field( 'text_block_heading' ) ); ?></h3>
				<?php the_sub_field( 'text_block_content' ); ?>
			<?php endwhile; ?>
		<?php } ?>
		</div><!--
		--><div class="col-item col-item-1-10 col-item--align-center col-item--vertical-divider col-item--hide--medium col-item--hide--small">
		</div><!--
		--><div class="col-item col-item-3-10 col-item-full--medium col-item-full--small">
			<?php if ( get_field( 'text_block_side_heading' ) ) { ?>
				<h3 class="uppercase"><?php the_field( 'text_block_side_heading' ); ?></h3>
			<?php }
			if ( get_field( 'text_block_side' ) ) {
				the_field( 'text_block_side' );
			} ?>
			<?php if ( get_field( 'accreditations_header' ) ) { ?>
				<h3 class="uppercase"><?php the_field( 'accreditations_header' ); ?></h3>
			<?php }
			if ( have_rows( 'accreditations' ) ): ?>
				<div class="col">
					<div class="col-item col-item-full col-item--align-center--small"><?php while ( have_rows( 'accreditations' ) ) : the_row(); ?>
				        <a href="<?php echo esc_attr( get_sub_field( 'logo_link' ) ); ?>" target="_blank"><img src="<?php echo esc_url( get_sub_field( 'logo' ) ); ?>" class="accreditation non-responsive" alt="<?php echo esc_attr( get_sub_field( 'logo_title' ) ); ?>" /></a>
					<?php endwhile; ?></div>
				</div><!-- .col -->
	    	<?php endif; ?>		
		</div>
	</div><!-- .col -->

</main><!-- #main -->
	
<?php get_footer(); ?>