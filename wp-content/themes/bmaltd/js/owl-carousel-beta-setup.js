jQuery(document).ready(function($){
    
    /*
     * Start Owl slider.
     */
    $("#owl-carousel-id").owlCarousel({

		items : 1,
		margin : 0,
		loop : true,
		center : false,
		mouseDrag : true,
		touchDrag : true,
		pullDrag : true,
		freeDrag : false,
		stagePadding : 0,
		merge : false,
		mergeFit : true,
		autoWidth : false,
		startPosition : 0,
		URLhashListener : false,
		nav : true,
		navRewind : true,
		navText : [],
		slideBy : 1,
		dots : false,
		dotsEach : false,
		dotData : false,
		lazyLoad : false,
		/* lazyContent : false, */
		autoplay : true,
		autoplayTimeout : 3000,
		autoplayHoverPause : false,
		/* smartSpeed : 250, */
		/*fluidSpeed : */
		autoplaySpeed : 500,
		navSpeed : false,
		dotsSpeed : 500,
		dragEndSpeed : 500,
		callbacks : true,
		responsive : true,
		responsiveRefreshRate : 0,
		responsiveBaseElement : window,
		responsiveClass : false,
		video : false,
		videoHeight : false,
		videoWidth : false,
		animateOut : false,
		animateIn : false,
		fallbackEasing : 'swing',
		info : false,
		nestedItemSelector : false,
		itemElement : 'div',
		stageElement : 'div',
		navContainer : false,
		dotsContainer : false

	})

});		