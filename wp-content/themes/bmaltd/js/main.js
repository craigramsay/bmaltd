/* ------------------------------------------------------------------------ *
 * JavaScript
 * ------------------------------------------------------------------------ */

/**
 * Mobile navigation.
 * Ensure mobile nav is always hidden when back / forward cache is used. 
 */
function pageShown(evt)
{
    if (evt.persisted) {

        $('.mobile-navigation').hide();

    }
}

if(window.addEventListener) {

  window.addEventListener("pageshow", pageShown, false);

} else {

  window.attachEvent("pageshow", pageShown, false);

}





/* ------------------------------------------------------------------------ *
 * jQuery
 * ------------------------------------------------------------------------ */

jQuery(document).ready(function($){

    /*
     * Toggle mobile navigation.
     */
    $('#mobile-navigation-navicon').click(function(event) {

        $('.mobile-navigation').toggleClass('mobile-navigation-show');
        $('body').toggleClass('mobile-navigation-show-body');

        event.preventDefault();

    });


    if ($('html').hasClass('lt-ie9')) {  
        $("img").each(function() {
            var src = $(this).attr("src");
            $(this).attr("src", src.replace(/\.svg$/i, ".png"));
        });
    }

    if ($('html').hasClass('lt-ie10')) {  
    
    };


    /*
     * Show fixed main navigation on scroll.
     * Scroll top should be no less than $navigation-height in SASS (120px).
     */    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('.wrap-full--full-fixed').addClass('wrap-full--full-fixed--js-show');
        } else {
            $('.wrap-full--full-fixed').removeClass('wrap-full--full-fixed--js-show');
        }
    });


    /*
     * Show fixed main navigation on scroll.
     */    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 55) {
            $('.mobile-header').addClass('mobile-header-scroll');
            $('.mobile-navigation-bar').addClass('mobile-navigation-bar-scroll');
            $('.mobile-navigation').addClass('mobile-navigation-scroll');
            //$('main').addClass('wrap-mobile--fixed');
        } else {
            $('.mobile-header').removeClass('mobile-header-scroll');
            $('.mobile-navigation-bar').removeClass('mobile-navigation-bar-scroll');
            $('.mobile-navigation').removeClass('mobile-navigation-scroll');
            //$('main').removeClass('wrap-mobile--fixed');
        }
    });


    /*
     * Mobile sidebar.
     */              
    $('.sidebar-mobile a.widget-open').click(function(e) {
        $('.sidebar-mobile').toggleClass('sidebar-mobile--open');
        $('.sidebar-mobile a.widget-open').toggleClass('widget-open--open');
        $('.sidebar-mobile aside.widget').slideToggle(400);
        e.preventDefault();
    });


    /*
     * Scroll to top.
     */    
    $(window).scroll(function () {
        if ($(this).scrollTop() > 300) {
            $('.back-top').fadeIn();
        } else {
            $('.back-top').fadeOut();
        }
    });


    /*
     * Scroll body to 0.
     */
    $('.back-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 600);
        return false;
    });
        

    /*
     * Internal scroll to.
     */         
    $('.int-scroll').click(function () {
        var e = $(this).attr('href');
        $('body,html').animate({
            scrollTop: $(e).offset().top-70
        }, 600);
        return false;
    });


    /*
     * Fade out success and error messages.
     * Use success-permanent and error-permanent otherwise.
     */
    $('.success, .error').delay(3000).fadeOut(800);

});