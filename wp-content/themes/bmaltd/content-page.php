<?php
/**
 * The template part for displaying a page in page.php.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> itemscope itemtype="http://schema.org/Article">

	<?php if ( get_field( 'title' ) ) { ?>
		<header class="content-header">
			<h1 class="content-header__title uppercase" itemprop="name"><?php echo esc_html( get_field( 'title' ) ); ?></h1>
		</header><!-- .content-header -->
	<?php } ?>

	<?php if ( get_the_content() ) { ?>
		<div class="col">
			<div class="col-item col-item-full col-item--margin-bottom-20">
				<div class="content-main" itemprop="articleBody">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	<?php } ?>

	<?php if ( have_rows( 'blocks' ) ): ?>
		<?php get_template_part( 'content', 'flexible-content' ); ?>
	<?php endif; ?>

	<?php echo malinky_content_hatom_footer(); ?>
	
	<?php if ( is_page( 'gallery' ) ) { ?>
		<h3 class="heading-1 uppercase">Video Gallery</h3>
		<video width="300" controls poster="<?php echo get_template_directory_uri(); ?>/img/school_construction_poster.jpg">
		  <source src="<?php echo home_url(); ?>/wp-content/uploads/school_construction.webm" type="video/webm">
		  <source src="<?php echo home_url(); ?>/wp-content/uploads/school_construction.mp4" type="video/mp4">
		  Your browser does not support the <code>video</code> element.
		</video>
		</div><!-- .col -->
	<?php } ?>
</article><!-- #post-## -->